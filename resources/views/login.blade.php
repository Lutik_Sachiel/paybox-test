<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Login page</title>
    <link rel="stylesheet" href="/css/bulma.min.css">
    <link rel="stylesheet" href="/css/app.css">
</head>
<body>
<section class="section">
    <div class="container">
        <div class="columns">
            <form action="{{ route('login') }}" method="post" class="column is-two-fifths">
                @csrf
                <h1 class="title">Введите ваши данные</h1>

                @isset ($errors)
                    @foreach ($errors->all() as $error)
                        <div class="message is-danger">
                            <div class="message-header">
                                <p>Danger</p>
                            </div>
                            <div class="message-body">{{ $error }}</div>
                        </div>
                    @endforeach
                @endif

                <div class="field">
                    <p class="control has-icons-left has-icons-right">
                        <input class="input" type="email" placeholder="admin@admin.com" name="email">
                        <span class="icon is-small is-left">
                  <i class="fas fa-envelope"></i>
                </span>
                        <span class="icon is-small is-right">
                  <i class="fas fa-check"></i>
                </span>
                    </p>
                </div>
                <div class="field">
                    <p class="control has-icons-left">
                        <input class="input" type="password" placeholder="1234" name="password">
                        <span class="icon is-small is-left">
                  <i class="fas fa-lock"></i>
                </span>
                    </p>
                </div>
                <div class="field">
                    <p class="control">
                        <button class="button is-success">
                            Login
                        </button>
                    </p>
                </div>
            </form>
        </div>
    </div>
</section>
</body>
</html>