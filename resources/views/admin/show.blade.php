<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
    <link rel="stylesheet" href="/css/bulma.min.css">
    <link rel="stylesheet" href="/css/app.css">
</head>
<body>
<section class="section">
    <div class="container">
        <nav class="breadcrumb" aria-label="breadcrumbs">
            <ul>
                <li><a href="/">Home</a></li>
                <li><a href="{{ route('admin.index') }}" aria-current="page">Payment list</a></li>
                <li class="is-active"><a aria-current="page">Payment #{{ $payment->id }}</a></li>
            </ul>
        </nav>
        <div class="content">
            <h3>Данные о платеже</h3>
            <div><b>ID:</b> {{ $payment->id }}</div>
            <div><b>Amount:</b> {{ $payment->amount }}тг.</div>
            <div><b>Status:</b> {{ $payment->status_label }}</div>
            <div><b>Created at:</b> {{ $payment->created_at->format('d.m.Y H:i:s') }}</div>
            @if ($payment->finished())
                <h3>Данные об оплате</h3>
                <div>
                    <b>Время оплаты:</b> {{ $payment->paid_at->format('d.m.Y H:i:s') }} <span class="tag is-primary">{{ $payment->paid_at->diffForHumans($payment->created_at) }}</span>
                </div>
                <div><b>Телефон плательщика:</b> {{ $payment->payer_phone }}</div>
                <div><b>Email:</b> {{ $payment->payer_email }}</div>
                <div><b>Карта:</b> {{ $payment->payer_card_hidden }}</div>
            @else
                <h3>Ссылка для оплаты</h3>
                <div class="field is-grouped">
                    <p class="control is-expanded">
                        <input class="input text-url" type="text" readonly value="{{ route('payment', ['payment_hash' => encrypt($payment->id)]) }}">
                    </p>
                    <p class="control">
                        <button class="button is-info button-copy">
                            Copy
                        </button>
                    </p>
                </div>
            @endif
        </div>
    </div>
</section>
<script>
    var btn = document.querySelector('.button-copy');
    btn.addEventListener('click', function(event) {
        var copyTextarea = document.querySelector('.text-url');
        copyTextarea.focus();
        copyTextarea.select();
        document.execCommand('copy');
        btn.innerText = 'Copied!';
    });
</script>
</body>
</html>