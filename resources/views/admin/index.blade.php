<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
    <link rel="stylesheet" href="/css/bulma.min.css">
    <link rel="stylesheet" href="/css/app.css">
</head>
<body>
<section class="section">
    <div class="container">
        <nav class="breadcrumb" aria-label="breadcrumbs">
            <ul>
                <li><a href="/">Home</a></li>
                <li class="is-active"><a href="#" aria-current="page">Payment list</a></li>
            </ul>
        </nav>
        <a class="button is-primary" href="{{ route('admin.add') }}">Выставить счет</a>
        <form class="is-grouped">
            <div>Статус: </div>
            <div class="select">
                <select name="status">
                    <option value="">Все</option>
                    @foreach(\App\Payment::statuses() as $id => $name)
                        <option value="{{ $id }}" {{ request()->input('status') === $id ? 'selected' : '' }}>{{ $name }}</option>
                    @endforeach
                </select>
            </div>
            <button class="button is-primary">Отправить</button>
        </form>
        <table class="table table-container table-responsive table-bordered">
            <thead>
            <tr>
                <th>ID</th>
                <th>Amount</th>
                <th>Status</th>
                <th>Paid at</th>
                <th>Created at</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($items as $item)
                <tr>
                    <td>{{ $item->id }}</td>
                    <td>{{ $item->amount }}</td>
                    <td>{{ $item->status_label }}</td>
                    <td>{{ $item->paid_at }}</td>
                    <td>{{ $item->created_at->format('d.m.Y H:i:s') }}</td>
                    <td><a class="button" href="{{ route('admin.show', ['payment' => $item->id]) }}">Show</a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $items->links() }}
    </div>
</section>
</body>
</html>