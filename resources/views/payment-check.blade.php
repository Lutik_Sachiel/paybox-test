<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
    <link rel="stylesheet" href="/css/bulma.min.css">
    <link rel="stylesheet" href="/css/app.css">
</head>
<body>
<section class="section">
    <div class="container">
        <div class="columns  is-centered">
            <div class="column is-3">

                <div style="padding: 10px 10px 10px 10px; border: 1px solid gray; border-radius: 5px;">

                    <nav class="level">
                        <div class="level-item">
                            <p style="text-align: center; font-weight: bold; color: #2d3436">{{ $payment->status_label }}</p>
                        </div>
                    </nav>

                    <nav class="level">

                        <div class="level-item">
                            <p style="text-align: center; font-size: 28px; font-weight: bold; color: #25c151">
                                {{ $payment->amount }}
                                <span style="color: gray; font-weight: normal; font-size: 20px;">тг.</span>
                            </p>
                        </div>
                    </nav>

                    <p style="text-align: center; font-size: 18px;">

                        @include('_partials.errors')
                        Оплачен {{ $payment->paid_at }}
                    {{--
                    <div>{{ \App\Payment::cutAmount(8.256) }}</div>
                    <div>{{ \App\Payment::cutAmount(10.1) }}</div>
                    <div>{{ \App\Payment::cutAmount(-0.128) }}</div>
                    --}}
                    </p>

                    <nav class="level" style="margin-top: 20px;">
                        <div class="level-item">
                            <div>
                                <button class="button" style="margin-top: 10px; background-color: #25c151; color: white; font-weight: bold; text-transform: uppercase;">
                                    Распечатать чек
                                </button>
                                <p style="text-align: center; font-size: 12px; margin-top: 5px;">
                                    просто кнопка :)
                                </p>
                            </div>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</section>
</body>
</html>