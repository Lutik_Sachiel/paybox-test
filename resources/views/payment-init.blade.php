<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
    <link rel="stylesheet" href="/css/bulma.min.css">
    <link rel="stylesheet" href="/css/app.css">
</head>
<body>
<section class="section">
    <div class="container">
        <div class="columns  is-centered">
            <div class="column is-3">
                @include('_partials.errors')
                <form action="{{ route('payment.process', ['payment_hash' => encrypt($payment->id)]) }}" style="padding: 10px 10px 10px 10px; border: 1px solid gray; border-radius: 5px;">
                    @csrf
                    <nav class="level">
                        <div class="level-item">
                            <p style="text-align: center; font-weight: bold; color: #2d3436">{{ $payment->status_label }}</p>
                        </div>
                    </nav>

                    <nav class="level">

                        <div class="level-item">
                            <p style="text-align: center; font-size: 28px; font-weight: bold; color: #25c151">
                                {{ $payment->amount }}
                                <span style="color: gray; font-weight: normal; font-size: 20px;">тг.</span>
                            </p>
                        </div>
                    </nav>

                    <div>
                        <div class="field">
                            <label class="label">Phone:</label>
                            <div class="control">
                                <input class="input" type="tel" placeholder="+7"  name="payer_phone" value="{{ old('payer_phone', $payment->payer_phone) }}" required>
                            </div>
                        </div>
                        <div class="field">
                            <label class="label">Email:</label>
                            <div class="control">
                                <input class="input" type="email" placeholder="email@email.com" name="payer_email" value="{{ old('payer_email', $payment->payer_email) }}" >
                            </div>
                        </div>
                        <div class="field">
                            <label class="label">Card number:</label>
                            <div class="control">
                                <input class="input" type="number" placeholder="" name="payer_card" required value="{{ old('payer_card') }}">
                            </div>
                        </div>
                        <label class="label">Срок действия карты:</label>
                        <div class="field is-grouped">
                            <div class="control" style="    flex-shrink: inherit;">
                                <input class="input" type="number" placeholder="{{ date('m') }}" name="payer_card_month" required min="{{ date('m') }}" max="12" value="{{ old('payer_card_month') }}">
                            </div>
                            <div class="control" style="    flex-shrink: inherit;">
                                <input class="input" type="number" placeholder="{{ date('Y') }}" name="payer_card_year" required min="{{ date('Y') }}" max="2030" value="{{ old('payer_card_year') }}">
                            </div>
                        </div>
                    </div>

                    <nav class="level" style="margin-top: 20px;">
                        <div class="level-item">
                            <div>
                                <button class="button" style="margin-top: 10px; background-color: #25c151; color: white; font-weight: bold; text-transform: uppercase;">
                                    Оплатить
                                </button>
                                <p style="text-align: center; font-size: 12px; margin-top: 5px;">
                                     Платеж №{{ $payment->id }}
                                </p>
                            </div>
                        </div>
                    </nav>
                </form>
            </div>
        </div>
    </div>
</section>
</body>
</html>