<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $guarded = ['id'];
    protected $dates = [
        'paid_at',
    ];
    const STATUS_PENDING = 'pending';
    const STATUS_SUCCESS = 'success';
    const STATUS_ERROR = 'error';

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->status = self::STATUS_PENDING;
        });
    }

    public static function statuses()
    {
        return [
            self::STATUS_PENDING => 'В ожидании оплаты',
            self::STATUS_SUCCESS => 'Оплата прошла успешно',
            self::STATUS_ERROR => 'Оплата завершена с ошибками',
        ];
    }

    public function getStatusLabelAttribute()
    {
        return $this->statuses()[$this->status] ?? '';
    }

    public function getPayerCardHiddenAttribute()
    {
        return substr($this->payer_card, 0, 6) . '******' . substr($this->payer_card,  -4);
    }

    public function payable()
    {
        return $this->status === self::STATUS_PENDING;
    }

    public function finished()
    {
        return $this->status === self::STATUS_SUCCESS || $this->status === self::STATUS_ERROR;
    }

    public static function cutAmount($amount)
    {
        return intval($amount * 100) / 100;

        [$num, $float] = explode('.', $amount);
        return sprintf('%s.%s%s',
            $num,
            $float[0] ?? 0,
            $float[1] ?? ''
        );
    }
}