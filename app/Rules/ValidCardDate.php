<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class ValidCardDate implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $day = '31';
        $year = request()->input('payer_card_year');
        $month = request()->input('payer_card_month') > 9 ? request()->input('payer_card_month') : '0'.request()->input('payer_card_month');
        $expiredDate = strtotime(sprintf('%s-%s-%s', $day, $month, $year));

        return time() < $expiredDate;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Card is expired';
    }
}
