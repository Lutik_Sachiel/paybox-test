<?php

namespace App\Http\Controllers;

use App\Payment;

class PaymentController
{
    public function __invoke(Payment $payment)
    {
        if ($payment->payable()) {
            return view('payment-init', compact('payment'));
        }

        return view('payment-check', compact('payment'));
    }
}