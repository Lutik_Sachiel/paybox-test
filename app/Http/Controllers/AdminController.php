<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorePaymentRequest;
use App\Payment;
use Illuminate\Http\Request;

class AdminController
{
    public function index(Request $request)
    {
        $items = Payment::query()
            ->when($request->filled('status'), function($query) {
                $query->where('status', request()->input('status'));
            })
            ->latest()
            ->paginate(10);

        return view('admin.index', compact('items'));
    }

    public function show(Payment $payment)
    {
        return view('admin.show', compact('payment'));
    }

    public function store(StorePaymentRequest $request)
    {
        $payment = Payment::create([
           'payer_phone' => $request->input('payer_phone'),
           'payer_email' => $request->input('payer_email'),
           'amount' => $request->input('amount'),
        ]);

        return redirect()->route('admin.show', ['payment' => $payment->id]);
    }
}