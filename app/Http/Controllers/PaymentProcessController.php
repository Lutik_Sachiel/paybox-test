<?php

namespace App\Http\Controllers;

use App\Payment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Rules\ValidCardDate;
use App\Rules\ValidCardNumber;

class PaymentProcessController
{
    public function process(Payment $payment, Request $request)
    {
        $payment->payer_phone = $request->input('payer_phone');
        $payment->payer_email = $request->input('payer_email');
        $payment->payer_card = $request->input('payer_card');
        $payment->paid_at = now();

        $validator = Validator::make($request->all(), [
            'payer_phone' => 'required',
            'payer_email' => 'email',
            'payer_card' => ['required', 'numeric', new ValidCardNumber, new ValidCardDate],
            'payer_card_month' => 'required|numeric|between:1,12',
            'payer_card_year' => 'required|numeric|between:2019,2030',
        ]);

        if ($validator->fails()) {
            $payment->status = Payment::STATUS_ERROR;
            $payment->save();

            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }
        $payment->status = Payment::STATUS_SUCCESS;
        $payment->save();

        return redirect()->back();
    }
}