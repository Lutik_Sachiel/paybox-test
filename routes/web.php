<?php

Route::group(['middleware' => 'guest'], function() {
    Route::view('/', 'login')->name('index');
    Route::post('/login', 'LoginController')->name('login');
});

Route::group(['middleware' => 'auth', 'prefix' => 'admin', 'as' => 'admin.'], function() {
    Route::get('/', 'AdminController@index')->name('index');
    Route::get('/{payment}/show', 'AdminController@show')->name('show');
    Route::view('/add', 'admin.add')->name('add');
    Route::post('/store', 'AdminController@store')->name('store');
});

Route::get('payment/{payment_hash}', 'PaymentController')->name('payment');
Route::get('payment/{payment_hash}/process', 'PaymentProcessController@process')->name('payment.process');